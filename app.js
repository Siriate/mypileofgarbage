const express = require('express')
const mongoose = require('mongoose')
const socket = require('socket.io')

mongoose.connect('mongodb://localhost/chat-app', { useNewUrlParser: true })

const Msg = require('./models/msg')

const app = express()

app.set('view engine', 'ejs')

app.get('/', function (req, res) {
    Msg.find().then((messages) => {
        console.log(messages)
        res.render('index', {chat: messages})
    })
})

const server = app.listen(3000, () => console.log('Server started at 3000'))

const io = socket(server);

io.on('connection', (socket) => {
    socket.on('msg', obj => {
        let newMsg = new Msg()
        newMsg.author = obj.author
        newMsg.text = obj.text
        newMsg.save((err) => {
            if (!err) {
                socket.emit('msg', obj);
            }
        });
    })
})